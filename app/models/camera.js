'use strict';
const {
    Model
} = require('sequelize');

const { appUrl, appPort } = require('../../config/app')

module.exports = (sequelize, DataTypes) => {
    class Camera extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    Camera.init({
    	camid: DataTypes.UUID,
        areaname: DataTypes.STRING,
        recording: DataTypes.BOOLEAN,
        detection: DataTypes.BOOLEAN,
        //true detection
        tdetection: DataTypes.BOOLEAN,
        //false detection
        fdetection: DataTypes.BOOLEAN,
        detections: DataTypes.INTEGER,
        zones: DataTypes.ENUM('green', 'orange', 'red'),
        records: {
            type: DataTypes.STRING,
            get() {
                const filename = this.getDataValue('video')
                const id = this.getDataValue('id')
                const url = `${appUrl}:${appPort}`

                if (filename)
                    return `${url}/records/videos/${id}/${filename}`

                return null
            }
        }
    }, {
        sequelize,
        modelName: 'Camera',
        tableName: 'cameras',
        underscored: true
    });
    return Camera;
};


